FROM node:11-alpine AS base
RUN mkdir /code
WORKDIR /code
COPY package.json package-lock.json /code/
RUN npm set progress=false && npm config set depth 0 && npm cache clean --force
RUN npm install --only=production

FROM base AS build
COPY . /code/
RUN npm run build

FROM base AS release
COPY --from=build /code/build ./build
CMD ["/code/node_modules/.bin/serve", "-s", "build"]
