const API_ROOT = 'http://localhost:8080/'

function callApi(endpoint) {
  const url = API_ROOT + endpoint

  return fetch(url)
    .then(response => {
      if (response.status >= 400) {
        throw new Error('Something went wrong')
      }
      return response.json()
    })
}

export const fetchAuthors = () => callApi(`authors/`)
export const fetchAllStats = () => callApi(`stats/`)
export const fetchAuthorsStats = (author_alias) => callApi(`stats/${author_alias}/`)
