import { SELECTED_AUTHORS_CHANGED } from "./types"

export const onChangeSelectedAuthors = (authorsList) => {
  const aliases = authorsList.map(author => author.value)
  return { type: SELECTED_AUTHORS_CHANGED, payload: aliases }
}
