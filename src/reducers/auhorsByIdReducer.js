import {
  FETCH_AUTHORS_SUCCEEDED,
  FETCH_AUTHORS_FAILED,
} from '../actions/types'

const authorsByIdReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_AUTHORS_SUCCEEDED:
      const authors = action.payload
      return Object.keys(authors).reduce(
        (memo, authorAlias) => {
          memo[authorAlias] = { name: authors[authorAlias], id: authorAlias }
          return memo
        },
        {}
      )
    case FETCH_AUTHORS_FAILED:
      console.log('There was an error while fetching authors list', action.payload)
      return state
    default:
      return state
  }
}

export default authorsByIdReducer
