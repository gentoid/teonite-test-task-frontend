import {
  FETCH_ALL_STATS_SUCCEEDED,
  FETCH_AUTHOR_STATS_SUCCEEDED,
} from "../actions/types"

const statsByAuthorsReducer = (state = { all: {} }, action) => {
  switch (action.type) {
    case FETCH_ALL_STATS_SUCCEEDED:
    return {
        ...state,
        all: action.payload,
    }
    case FETCH_AUTHOR_STATS_SUCCEEDED:
      return {
        ...state,
        [action.payload.authorAlias]: action.payload.data,
      }
    default:
      return state
  }
}

export default statsByAuthorsReducer
