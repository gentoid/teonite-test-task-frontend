import { SELECTED_AUTHORS_CHANGED } from '../actions/types'

const selectedAuthorsReducer = (state = [], action) => {
  switch (action.type) {
    case SELECTED_AUTHORS_CHANGED:
      return action.payload
    default:
      return state
  }
}

export default selectedAuthorsReducer
