import { combineReducers } from 'redux'

import authorsByIdReducer from './auhorsByIdReducer'
import selectedAuthorsReducer from './selectedAuthorsReducer'
import statsByAuthorsReducer from './statsByAuthorsReducer'


export default combineReducers({
  authorsById: authorsByIdReducer,
  selectedAuthors: selectedAuthorsReducer,
  statsByAuthors: statsByAuthorsReducer,
})
