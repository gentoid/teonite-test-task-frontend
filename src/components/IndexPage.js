import React from 'react'

const HomePage = () => (
  <div>
    <h2>Welcome to our super cool statistics service</h2>
    <article>You gonna have a wonderful experience while using the app we've built! Enjoy!</article>
  </div>
)

export default HomePage
