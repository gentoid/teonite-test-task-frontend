import React from 'react'
import ReactSelect from 'react-select'
import PropTypes from 'prop-types'

const Select = ({ options, onChange, value }) => (
  <ReactSelect
    isMulti
    onChange={onChange}
    options={options}
    value={value}
  />
)

const optionsType = PropTypes.arrayOf(
  PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
).isRequired

Select.propTypes = {
  options: optionsType,
  onChange: PropTypes.func.isRequired,
  value: optionsType,
}

export default Select
