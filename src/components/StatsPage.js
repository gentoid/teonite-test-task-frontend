import React from 'react'
import SelectAuthors from '../containers/SelectAuthors'
import StatsTable from '../containers/StatsTable'

const StatsPage = () => (
  <section>
    <SelectAuthors />
    <br/>
    <StatsTable />
  </section>
)

export default StatsPage
