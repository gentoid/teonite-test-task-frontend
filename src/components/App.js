import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import './App.css'

import IndexPage from './IndexPage'
import StatsPage from './StatsPage'

const App = () => (
  <Router>
    <div className="App">
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/stats/">Statistics</Link>
          </li>
        </ul>
      </nav>

      <Route path="/" exact component={IndexPage} />
      <Route path="/stats/" component={StatsPage} />
    </div>
  </Router>
)

export default App
