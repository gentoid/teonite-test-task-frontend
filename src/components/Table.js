import React from 'react'
import ReactTable from 'react-table'
import PropTypes from 'prop-types'

import 'react-table/react-table.css'

const columns = [{
  Header: 'Word',
  accessor: 'word',
}, {
  Header: 'Count',
  accessor: 'count',
}]

const Table = ({ data }) => (
  <ReactTable
    data={data}
    columns={columns}
    minRows={0}
    showPagination={false}
  />
)

Table.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      word: PropTypes.string.isRequired,
      count: PropTypes.number.isRequired,
    })
  ).isRequired,
}

export default Table
