import { connect } from 'react-redux'

import Select from '../components/Select'
import { onChangeSelectedAuthors } from '../actions/selectedAuthors'

const transformToOption = author => ({ value: author.id, label: author.name })

const prepareOptions = authorsById => {
  return Object.values(authorsById)
    .map(transformToOption)
}

const prepareSelectedValues = (authorsById, selectedAliases) => {
  return selectedAliases
    .map(alias => authorsById[alias])
    .map(transformToOption)
}

const mapStateToProps = state => ({
  options: prepareOptions(state.authorsById),
  value: prepareSelectedValues(state.authorsById, state.selectedAuthors),
})

const mapDispatchToProps = dispatch => ({
  onChange: authors => dispatch(onChangeSelectedAuthors(authors)),
})

const SelectAuthors = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Select)

export default SelectAuthors
