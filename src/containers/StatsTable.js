import { connect } from 'react-redux'

import Table from '../components/Table'

const tranform = hash => {
  return Object.keys(hash)
    .map(word => ({ word: word, count: hash[word] }))
}

const mergeTwoStats = (statsOne, statsTwo) => {
  return Object.keys(statsOne).reduce(
    (memo, word) => {
      memo[word] = (memo[word] || 0) + statsOne[word]
      return memo
    },
    { ...statsTwo }, // We need to create a copy, otherwise we'll modify the original data
  )
}

const mergeMultipleStats = statsList => {
  return statsList.reduce(
    (memo, stats) => mergeTwoStats(memo, stats),
    {},
  )
}

const getStats = (selectedAuthors, statsByAuthors) => {
  if (selectedAuthors.length < 1) {
    return tranform(statsByAuthors.all)
  }

  const selectedStats = selectedAuthors.map(authorAlias => statsByAuthors[authorAlias])
  return tranform(mergeMultipleStats(selectedStats))
}

const mapStateToProps = state => {
  return { data: getStats(state.selectedAuthors, state.statsByAuthors) }
}

const StatsTable = connect(
  mapStateToProps,
  null,
)(Table)

export default StatsTable
