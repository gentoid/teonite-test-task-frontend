import {
  call,
  put,
  takeLatest
} from 'redux-saga/effects'

import * as Api from '../services/api'
import {
  FETCH_AUTHORS_REQUESTED,
  FETCH_AUTHORS_SUCCEEDED,
  FETCH_AUTHORS_FAILED,
} from '../actions/types'


function* fetchAuthorsList(_action) {
  try { // TODO: Are there monads in JS world?
    const data = yield call(Api.fetchAuthors)
    yield put({ type: FETCH_AUTHORS_SUCCEEDED, payload: data })
  } catch (error) {
    yield put({ type: FETCH_AUTHORS_FAILED, payload: error })
  }
}

function* watchFetchAuthorsList() {
  yield takeLatest(FETCH_AUTHORS_REQUESTED, fetchAuthorsList)
}

export default watchFetchAuthorsList
