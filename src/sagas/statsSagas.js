import {
  call,
  put,
  select,
  takeEvery,
} from 'redux-saga/effects'

import * as Api from '../services/api'
import {
  FETCH_AUTHOR_STATS_REQUESTED,
  FETCH_AUTHOR_STATS_SUCCEEDED,
  FETCH_AUTHOR_STATS_FAILED,
  FETCH_ALL_STATS_REQUESTED,
  FETCH_ALL_STATS_SUCCEEDED,
  FETCH_ALL_STATS_FAILED,
  SELECTED_AUTHORS_CHANGED,
} from '../actions/types'

const cachedAuthorStats = (state, authorAlias) => state.statsByAuthors[authorAlias]

function* authorsStatsSaga(action) {
  try {
    const authorAlias = action.authorAlias
    const cachedData = yield select(cachedAuthorStats, authorAlias)
    const data = cachedData || (yield call(Api.fetchAuthorsStats, authorAlias))

    yield put({ type: FETCH_AUTHOR_STATS_SUCCEEDED, payload: { data, authorAlias } })
  } catch (error) {
    yield put({ type: FETCH_AUTHOR_STATS_FAILED, payload: error })
  }
}

function* allStatsSaga() {
  try {
    const data = yield call(Api.fetchAllStats)
    yield put({ type: FETCH_ALL_STATS_SUCCEEDED, payload: data })
  } catch (error) {
    yield put({ type: FETCH_ALL_STATS_FAILED, payload: error })
  }
}

function* statsForSelectedAuthorsSaga(action) {
  for (var authorAlias of action.payload) {
    yield call(authorsStatsSaga, { authorAlias })
  }
}

function* watchStatsSagas() {
  yield takeEvery(FETCH_AUTHOR_STATS_REQUESTED, authorsStatsSaga)
  yield takeEvery(FETCH_ALL_STATS_REQUESTED, allStatsSaga)
  yield takeEvery(SELECTED_AUTHORS_CHANGED, statsForSelectedAuthorsSaga)
}

export default watchStatsSagas
