import { put } from 'redux-saga/effects'

import {
  FETCH_AUTHORS_REQUESTED,
  FETCH_ALL_STATS_REQUESTED,
} from '../actions/types'


function* startApp() {
  yield put({ type: FETCH_AUTHORS_REQUESTED })
  yield put({ type: FETCH_ALL_STATS_REQUESTED })
}

export default startApp
