import { all } from 'redux-saga/effects'

import startApp from './startApp'
import watchFetchAuthorsList from './authorsList'
import watchStatsSagas from './statsSagas'
import watchAndLog from './loggerSaga'


const rootSaga = function* () {
  yield all([
    watchAndLog(),
    watchFetchAuthorsList(),
    watchStatsSagas(),
    startApp(), // this call should be the latest, because all of the wathers should already be ready
  ])
}

export default rootSaga
